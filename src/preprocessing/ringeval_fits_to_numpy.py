"""
By Razvan Ciuca, 2017

This simple script takes the .fits files from ringeval and converts them to usable numpy arrays.
This script is only called from map_preprocess_pytorch.py if it detects that the numpy arrays are not already present

"""

import numpy as np
from astropy.io import fits
from config import *


# converts a fits file to a numpy array
def fits_to_numpy(filename):
    return fits.open(filename)[0].data

# empty arrays in which the maps will be stored
g_maps = []
s_maps = []
a_maps = []

# for each file, change to numpy and append to the corresponding array
for n in range(100):

    g_name = ringeval_fits_folder_name + 'dTmap_cdm_Rlen' + format(n, "04") + '.fits'
    s_name = ringeval_fits_folder_name + 'dTmap_stg_Dcmb' + format(n, "04") + '.fits'
    a_name = ringeval_fits_folder_name + 'slc_stg_Dcmb' + format(n, "04") + '.fits'

    # these have a singleton first dimension to make it easier to concatenate them together afterwards
    g_maps.append(fits_to_numpy(g_name).reshape([1, 1024, 1024]))
    s_maps.append(fits_to_numpy(s_name).reshape([1, 1024, 1024]))
    a_maps.append(fits_to_numpy(a_name).reshape([1, 1024, 1024]))

# Concatenate the numpy arrays in the lists, which makes them arrays of size [100, 1024, 1024] and save them to file
np.save(save_size_1024_ringeval_g_maps_to, np.concatenate(g_maps, 0))
np.save(save_size_1024_ringeval_s_maps_to, np.concatenate(s_maps, 0))
np.save(save_size_1024_ringeval_a_maps_to, np.concatenate(a_maps, 0))





