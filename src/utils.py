"""
by Razvan Ciuca, 2017
This script contains generally useful functions

"""


import numpy as np
import torch as t
import scipy.misc
import numpy.fft as fft
import pickle
import os
import torch.nn as nn
from torch.autograd import Variable
from model_def import Net


def constant_bin_height_histogram(m, n_bins):

    # total number of elements
    N = np.prod(m.shape)
    n_per_bin = int(N/n_bins)
    y = np.sort(m.flatten())
    output_intervals = []

    for i in range(0, n_bins):
        k = i*n_per_bin
        output_intervals.append(y[k])
    output_intervals.append(y[-1])

    return [n_per_bin/(output_intervals[i+1]-output_intervals[i])/N for i in range(0, n_bins)], output_intervals


def constant_bin_height_histogram_pytorch(m, n_bins):

    # total number of elements
    N = np.prod(list(m.size()))
    n_per_bin = int(N/n_bins)
    y, indices = t.sort(m.view(-1))
    output_intervals = []

    for i in range(0, n_bins):
        k = i*n_per_bin
        output_intervals.append(y[k])
    output_intervals.append(y[-1])

    return [n_per_bin/(output_intervals[i+1]-output_intervals[i])/N for i in range(0, n_bins)], output_intervals



# This returns a map with the top k pixels set to 1
def get_top_k_pixels(m, k):

    result = np.zeros(m.shape)
    p = np.argpartition(-m.squeeze(), k, axis=None)[:k]

    for index in p:
        i = index % m.shape[0]
        j = int((index - i)/m.shape[1])
        result[j][i] = 1

    return result


# This simply returns x with mean set to 0 and std set to 1
def normalize_map(x):
    return (x-x.mean())/x.std()


# this returns an array with
def set_border_to_minimum(x, border_size, numpy=False):

    result = x.squeeze()
    minimum = result[border_size:-border_size, border_size:-border_size].min() if numpy else \
        result[border_size:-border_size, border_size:-border_size].min()[0]
    result[:border_size] = minimum
    result[-border_size:] = minimum
    result[:, :border_size] = minimum
    result[:, -border_size:] = minimum

    return result


# this pads the image by pad_size using repeated pixels
def pad_repeating(x, pad_size):
    s2 = x.size(2)
    s3 = x.size(3)
    x1 = t.cat([x[:, :, s2 - pad_size: s2], x, x[:, :, 0:pad_size]], 2)
    x2 = t.cat([x1[:, :, :, s3 - pad_size: s3], x1, x1[:, :, :, 0:pad_size]], 3)
    return x2


# takes the gradient of of the maps, input maps must be torch maps
def get_gradient(input_maps, numpy=False):

    maps = input_maps if not numpy else Variable(t.from_numpy(input_maps))

    maps = pad_repeating(maps, 1)

    # Sobel operators for gradients
    Wx = [[[[-1, 0, 1],
            [-2, 0, 2],
            [-1, 0, 1]]]]

    Wy = [[[[1, 2, 1],
            [0, 0, 0],
           [-1, -2, -1]]]]

    convx = nn.Conv2d(1, 1, 3, 1, 0, bias=False)
    convy = nn.Conv2d(1, 1, 3, 1, 0, bias=False)

    convx.weight = nn.Parameter(t.Tensor(Wx))
    convy.weight = nn.Parameter(t.Tensor(Wy))

    gradient = t.sqrt(t.pow(convx.forward(maps), 2)+t.pow(convy.forward(maps), 2))

    minimum = gradient.min().data[0]
    gradient[:, :, :1] = minimum
    gradient[:, :, -1:] = minimum
    gradient[:, :, :, :1] = minimum
    gradient[:, :, :, -1:] = minimum

    return gradient.data.numpy() if numpy else gradient


# this writes the image in cmbedge format, if write_png==True, also creates a png of the numpy image
def write_image_cmbedge_format(map, filename, write_png=False):
    x = map.squeeze()
    with open(filename, 'w') as file:
        for i in range(0, x.shape[0]):
            for j in range(0, x.shape[1]):
                file.write(str(j) + '\t' + str(i) + '\t' + str(x[j, i]) + '\n')
        file.close()
    if write_png:
        scipy.misc.imsave(filename.split('.dat')[0] + '.png', x)


# loads a cmbedge file and returns a numpy array
def numpy_from_cmbedge_file(filename, size=512):
    map = np.ndarray([size, size], dtype=np.float32)
    with open(filename) as file:
        for line in file:
            if line != '':
                l = line.split('\t')
                map[int(l[0]), int(l[1])] = float(l[2])

    return map


# these are routines to compute the power spectrum of numpy maps
def azimuthal_average(image, center=None):
    """
    Calculate the azimuthally averaged radial profile.

    image - The 2D image
    center - The [x,y] pixel coordinates used as the center. The default is 
             None, which then uses the center of the image (including 
             fracitonal pixels).

    """
    # Calculate the indices from the image
    y, x = np.indices(image.shape)

    if not center:
        center = np.array([(x.max() - x.min()) / 2.0, (x.max() - x.min()) / 2.0])

    r = np.hypot(x - center[0], y - center[1])

    # Get sorted radii
    ind = np.argsort(r.flat)
    r_sorted = r.flat[ind]
    i_sorted = image.flat[ind]

    # Get the integer part of the radii (bin size = 1)
    r_int = r_sorted.astype(int)

    # Find all pixels that fall within each radial bin.
    deltar = r_int[1:] - r_int[:-1]  # Assumes all radii represented
    rind = np.where(deltar)[0]  # location of changed radius
    nr = rind[1:] - rind[:-1]  # number of radius bin

    # Cumulative sum to figure out sums for each radius bin
    csim = np.cumsum(i_sorted, dtype=float)
    tbin = csim[rind[1:]] - csim[rind[:-1]]

    radial_prof = tbin / nr

    return radial_prof


# maps has size [n_maps, size, size], this will return an array of length [n_maps, floor(size/2)] containing the power spectrum
# of each map
def power_spectrum(maps, return_fft_maps=False):
    """
    given an array of maps of size (n_maps, length, length)
    this produces the power spectrum of each of the n_maps,
    """

    power_spectra = []

    map_size = maps.shape[1]
    fft_maps = np.abs(fft.fftshift(fft.fft2(maps)))**2

    for i in range(0, maps.shape[0]):
        power_spectra.append(azimuthal_average(fft_maps[i])[0:int(map_size/2)])

    if return_fft_maps:
        return np.array(power_spectra), fft_maps
    else:
        return np.array(power_spectra)


# This function agglomerates all the models saved in a directory, returns them and saves a pickled copy
def agglomerate_models(models_dir='../models/cmbedge/'):
    noise_dir_list = os.listdir(models_dir)

    model_dict = {}

    if 'string_making' in noise_dir_list:
        noise_dir_list.remove('string_making')

    if 'all_models_backup' in noise_dir_list:
        noise_dir_list.remove('all_models_backup')

    if 'all_models' in noise_dir_list:
        noise_dir_list.remove('all_models')

    for noise_dir in noise_dir_list:
        Gmu_dir_list = os.listdir(models_dir + noise_dir)
        Gmu_dict = {}

        for Gmu_dir in Gmu_dir_list:

            models_list = os.listdir(models_dir + noise_dir + '/' + Gmu_dir)
            print('doing ' + noise_dir + ' ' + Gmu_dir)
            last_model = models_list[0]
            current_iteration = 0

            for model in models_list:
                iteration = int(model.split('_')[2])
                if iteration > current_iteration:
                    last_model = model
                    current_iteration = iteration

            net = Net()
            load_from = models_dir + noise_dir + '/' + Gmu_dir + '/' + last_model
            net.load_state_dict(t.load(load_from, map_location=lambda storage, loc: storage))

            Gmu_dict[float(Gmu_dir)] = net

        noise = float(noise_dir.split('_')[2])
        model_dict[noise] = Gmu_dict

    pickle.dump(model_dict, open(models_dir + 'all_models', 'wb'))
    return model_dict
